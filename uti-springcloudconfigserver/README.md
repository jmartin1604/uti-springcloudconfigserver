### Template Parameters
| Name | Value |
| ------ | ------ |
| Nombre de la aplicacion | springcloudconfigserver |
| URL de bitbucket | https://bitbucket.org/VisaNet_TI/uti-springcloudconfigserver.git |
| Branch | developer |
| Contexto del servicio | /config-client  |
| Git Secret | mdp-crios-bitbucket |
| Target Port | 8889 |
| URL de servicio | |
| Maven Arguments | package -DskipTests -Dfabric8.skip -e -B |
| CPU request | 0.05 |
| CPU limit | 0.5 |
| % Memoria utilizar | 200M |
| Memoria limit | 250M |