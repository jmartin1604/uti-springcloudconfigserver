package pe.com.visanet.controller;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.com.visanet.jms.StomJMS;
import pe.com.visanet.util.Utilitarios;

@RestController
@RequestMapping
public class MonitorController {

	private static final Logger logger = LoggerFactory.getLogger(MonitorController.class);

	RestTemplate restTemplate;

	public MonitorController() {
		restTemplate = new RestTemplate();
	}

	@Autowired
	private Environment environment;
	
	@Autowired
	private StomJMS stomJMS;
	

	@GetMapping(value = "/healthcheck", produces = "application/json; charset=UTF-8")
	public String obtenerHealthCheck() {
		return "{\"status\":\"UP\"}";
	}

	public String obtenerSecrets() {
		String endpoint = environment.getProperty("spring.cloud.config.uri");
		String response = restTemplate.getForObject(endpoint, String.class);
		String responseSplit = response.split("\"source\":")[1];
		responseSplit = responseSplit.substring(0, responseSplit.length() - 3);
		return responseSplit;
	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/monitor", produces = "application/json; charset=UTF-8")
	public void procesarConfigProperties() {
		final String json = obtenerSecrets();
		final Gson gson = new Gson();
		final Map<String, String> map = gson.fromJson(json, HashMap.class);
		for (Entry<String, String> p : map.entrySet()) {
			String key = p.getKey();
			logger.info("key: {}", key);
			final Map<String, String> val = gson.fromJson(gson.toJson(p.getValue()), HashMap.class);
			for (Entry<String, String> ep : val.entrySet()) {
				if ("value".equals(ep.getKey()))
					logger.info("value: {}", ep.getValue());
			}
		}
	}

	@GetMapping(value = "/obtenerEnv", produces = "application/json; charset=UTF-8")
	public void obtenerEnv() {
		try {
			Map<String, String> map = System.getenv();
			List<String> listValueSecret = new ArrayList<>();
			for (Map.Entry<String, String> entry : map.entrySet()) {
//				if(entry.getKey().contains("PUBLIC")) {
				if(entry.getKey().endsWith("client.js")) {
//				String value = "const axios = require(\"axios\");\r\n" + 
//						"const qs = require(\"qs\");\r\n" + 
//						"\r\n" + 
//						"\r\n" + 
//						"const { configure, getLogger } = require('log4js');\r\n" + 
//						"configure('./config/log4js.json');\r\n" + 
//						"const logger = getLogger();\r\n" + 
//						"logger.level = 'debug';\r\n" + 
//						"async function cliente(body, transactionId) {\r\n" + 
//						"    logger.debug(` >[banbifClientResponse] [${transactionId}]- INICIO`);\r\n" + 
//						"\r\n" + 
//						"    var banbifClientResponse = new Object();\r\n" + 
//						"    var { url, timeout, destinationName } = body.credentials;\r\n" + 
//						"    body.dataMapping[0].data =qs.stringify(body.dataMapping[0].data);\r\n" + 
//						"    body.dataMapping[0].timeout=timeout\r\n" + 
//						"    banbifClientResponse.app = destinationName;\r\n" + 
//						"    body.dataMapping[0].url=url\r\n" + 
//						"\r\n" + 
//						"\r\n" + 
//						"    try {\r\n" + 
//						"        let starShipInfo = await axios(body.dataMapping[0])\r\n" + 
//						"        const starShipInfoData = starShipInfo.data;\r\n" + 
//						"        banbifClientResponse.status = \"00\";\r\n" + 
//						"        banbifClientResponse.mensaje = starShipInfoData;\r\n" + 
//						"        logger.debug(` >[banbifClientResponse] [${transactionId}] - [${JSON.stringify(starShipInfoData)}]`);\r\n" + 
//						"\r\n" + 
//						"    } catch (error) {\r\n" + 
//						"        //console.log(error.json())\r\n" + 
//						"        banbifClientResponse.status = \"ERROR\";\r\n" + 
//						"        if(error.response===undefined){\r\n" + 
//						"            banbifClientResponse.mensaje = error.message;\r\n" + 
//						"        }else{\r\n" + 
//						"            banbifClientResponse.mensaje = error.response.data;\r\n" + 
//						"\r\n" + 
//						"        }\r\n" + 
//						"        //console.log(JSON.stringify(error.response.data))\r\n" + 
//						"        //logger.debug(` >[banbifClientResponse] [${transactionId}] - [${JSON.stringify(error.response.data)}]`);\r\n" + 
//						"    }\r\n" + 
//						"\r\n" + 
//						"    logger.debug(` >[banbifClientResponse] [${transactionId}] - FIN`);\r\n" + 
//						"    return await banbifClientResponse;\r\n" + 
//						"}\r\n" + 
//						"module.exports = {\r\n" + 
//						"    cliente: cliente,\r\n" + 
//						"}";
//					byte[] valueBytes = value.getBytes(StandardCharsets.UTF_8);
					logger.info("Variable Name:- " + entry.getKey() + " Value:- " + entry.getValue());					
					listValueSecret.add(Utilitarios.concatStr(entry.getKey(), entry.getValue()));					
//					logger.info("Byte value: {}", valueBytes);
				}			
			}
			if (!listValueSecret.isEmpty())
				stomJMS.enviarJMS(listValueSecret);
		} catch (Exception e) {
			logger.error("Error: {}", e);
		}
	}

}
