package pe.com.visanet.jms;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.activemq.transport.stomp.StompConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.visanet.util.Constantes;
import pe.com.visanet.util.Properties;

@Service
public class StomJMSImpl implements StomJMS {
	
	private static final Logger LOG = LoggerFactory.getLogger(StomJMSImpl.class);	

	@Autowired
	private Properties properties;

	public void enviarJMS(List<String> listValueSecret) {

		String transaccionId = String.valueOf(UUID.randomUUID().getMostSignificantBits());
		StompConnection connection = new StompConnection();
		String nombreMetodo = "[enviarJMS]";

		try {
			LOG.info("[INICIO] - METODO: {}" + nombreMetodo);
			LOG.info("Enviado mensaje a la cola, destino {} ", properties.destination);
			LOG.info("Enviado mensaje a la cola, host {} ", properties.host);
			LOG.info("Enviado mensaje a la cola, puerto {} ", properties.port);
			
			connection.open(properties.host, properties.port);
			connection.connect(properties.user, properties.password);
			connection.begin(transaccionId);

			for (String lst : listValueSecret) {
				connection.send(properties.destination, lst);
				LOG.info("Value secret enviado: {}", lst);
			}
			connection.commit(transaccionId);

		} catch (Throwable t) {
			LOG.error(t.getMessage());
		} finally {
			try {
				connection.close();
			} catch (IOException e) {
				LOG.error(Constantes.VACIO + e);
			}
		}
	}
}
