package pe.com.visanet.util;

public class Constantes {
	
	private Constantes() {
		super();
	}

	public static final String VACIO = "";
	public static final String DOS_PUNTOS = ":";
	public static final String PALOTE = "\\|";
	public static final String IGUAL = "=";
	
}
