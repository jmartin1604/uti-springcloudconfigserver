package pe.com.visanet.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

	private Properties() {
		super();
	}

//	@Value("${spring.cloud.config.uri}")
//	public String endpoint;

	@Value("${stom.client.host}")
	public String host;

	@Value("${stom.client.port}")
	public int port;

	@Value("${stom.client.user}")
	public String user;

	@Value("${stom.client.password}")
	public String password;
	
	@Value("${stom.destination.queue}")
	public String destination;

}
