package pe.com.visanet.util;

public class Utilitarios {

	private Utilitarios() {
		super();
	}

	public static String concatStr(String val1, String val2) {
		return null != val1 && null != val2 ? val1.concat(Constantes.DOS_PUNTOS.concat(val2)) : Constantes.VACIO;
	}

}
